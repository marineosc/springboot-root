package com.young.mybatisdiy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisDiyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisDiyApplication.class, args);
    }

}
