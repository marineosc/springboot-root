package com.young.redis.event;

import org.springframework.context.ApplicationEvent;

/**
 * 事件监听-事件定义
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/11/25
 */
public class RedisShutDownEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public RedisShutDownEvent(Object source) {
        super(source);
    }
}
