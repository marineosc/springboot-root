package com.young.redis.event.listener;

import com.young.redis.event.RedisShutDownEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Spring事件监听-处理
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/11/25
 */
@Component
public class ApplicationRedisShutDownEventListener implements ApplicationListener<RedisShutDownEvent> {

    private static Logger logger = LogManager.getLogger(ApplicationRedisShutDownEventListener.class);

    @Autowired
    RedissonClient redissonClient;

    @Override
    public void onApplicationEvent(RedisShutDownEvent event) {
        redissonClient.shutdown();
        logger.info(event.getSource());
    }
}
