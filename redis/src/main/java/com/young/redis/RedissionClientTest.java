package com.young.redis;

import com.young.redis.config.RedissonConfig;
import com.young.redis.event.RedisShutDownEvent;
import com.young.redis.event.listener.ApplicationRedisShutDownEventListener;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Redission连接测试
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/11/20
 */
public class RedissionClientTest {
    public static void main(String[] args) throws InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(RedissonConfig.class);
        context.register(RedissionClientTest.class);
        context.register(ApplicationRedisShutDownEventListener.class);
        context.refresh();
        RedissonClient redissonClient = context.getBean(RedissonClient.class);
        RMap<String, String> rMap = redissonClient.getMap("demo");
        System.out.printf("Map容量：%d\n", rMap.size());
        System.out.printf("test：%s\n", rMap.get("test"));
        context.publishEvent(new RedisShutDownEvent("关闭Redis"));
        Thread.sleep(30000);
        context.close();
    }
}
