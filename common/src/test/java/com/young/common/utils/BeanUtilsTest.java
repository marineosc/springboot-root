package com.young.common.utils;

import com.young.common.entity.User;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BeanUtilsTest {
    private static final int count = 1000;

    @Test
    public void copyProperties() throws IOException {
        System.out.println(String.format("拷贝数据:%d条", count));
        this.copyBySerialize();
        this.copyByReflex();
        this.copyByMethod();
    }

    private List<UserMirror> copyBySerialize() throws IOException {
        long begin = System.currentTimeMillis();
        User user = new User();
        user.setName("Young");
        user.setSex("male");
        List<UserMirror> list = new ArrayList<>(count);
        for (int index = 0; index < count; index++) {
            list.add(BeanUtils.copyProperties(user, UserMirror.class));
        }
        long end = System.currentTimeMillis();
        System.out.println(String.format("序列化反序列化方式耗时:%d millis", end - begin));
        return list;
    }

    private List<UserMirror> copyByReflex() throws IOException {
        long begin = System.currentTimeMillis();
        User user = new User();
        user.setName("Young");
        user.setSex("male");
        List<UserMirror> list = new ArrayList<>(count);
        for (int index = 0; index < count; index++) {
            UserMirror userMirror = new UserMirror();
            org.springframework.beans.BeanUtils.copyProperties(user, userMirror);
            list.add(userMirror);
        }
        long end = System.currentTimeMillis();
        System.out.println(String.format("反射方式耗时:%d millis", end - begin));
        return list;
    }

    /**
     * Getter/Setter
     *
     * @return
     * @throws IOException
     */
    private List<UserMirror> copyByMethod(){
        long begin = System.currentTimeMillis();
        User user = new User();
        user.setName("Young");
        user.setSex("male");
        List<UserMirror> list = new ArrayList<>(count);
        for (int index = 0; index < count; index++) {
            UserMirror userMirror = new UserMirror();
            userMirror.setName(user.getName());
            userMirror.setSex(user.getSex());
            list.add(userMirror);
        }
        long end = System.currentTimeMillis();
        System.out.println(String.format("Getter/Setter方式耗时:%d millis", end - begin));
        return list;
    }

}