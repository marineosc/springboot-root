package com.young.common.utils;

import com.young.common.entity.Account;
import com.young.common.entity.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionsUtilsTest {
    /**
     * {@link CollectionsUtils#copyOf}
     * 测试
     */
    @Test
    public void copyOf() {
        User source = new User();
        source.setName("young");
        source.setSex("male");
        List<User> sourceList = new ArrayList<>(1);
        sourceList.add(source);
        List<User> targetList = CollectionsUtils.copyOf(sourceList, User.class);
        User target = targetList.get(0);
        target.setSex("female");
        System.out.println(String.format("copyOf#source:%s,target:%s", source.toString(), target.toString()));
    }

    @Test
    public void apiCopyOf() {
        User source = new User();
        source.setName("young");
        source.setSex("male");
        List<User> sourceList = new ArrayList<>(1);
        sourceList.add(source);
        List<Object> targetList = Arrays.asList(Arrays.copyOf(sourceList.toArray(), sourceList.size()));
        User target = User.class.cast(targetList.get(0));
        target.setSex("female");
        System.out.println(String.format("apiCopyOf#source:%s,target:%s", source.toString(), target.toString()));
    }
    @Test
    public void streamCopyOf() {
        User source = new User();
        source.setName("young");
        source.setSex("male");
        List<User> sourceList = new ArrayList<>(1);
        sourceList.add(source);
        List<User> targetList = sourceList.stream().collect(Collectors.toList());
        User target = targetList.get(0);
        target.setSex("female");
        System.out.println(String.format("streamCopyOf#source:%s,target:%s", source.toString(), target.toString()));
    }

    /**
     * 对象引用测试
     */
    @Test
    public void referenceTest() {
        User source = new User();
        source.setName("young");
        source.setSex("male");
        User target = source;
        target.setSex("female");
        System.out.println(String.format("source:%s,target:%s", source.toString(), target.toString()));
        Assert.assertTrue(source.getSex().equals(target.getSex()));
    }

    /**
     * 对象克隆测试
     *
     * @throws CloneNotSupportedException
     */
    @Test
    public void cloneTest() throws CloneNotSupportedException {
        User source = new User();
        source.setName("young");
        source.setSex("male");
        User target = User.class.cast(source.clone());
        target.setSex("female");
        System.out.println(String.format("source:%s,target:%s", source.toString(), target.toString()));
        Assert.assertTrue(source.getSex().equals(target.getSex()));
    }

    @Test
    public void cloneReferenceTest() throws CloneNotSupportedException {
        Account source = new Account();
        source.accountId = "source.accountId";
        source.password = "source.password";
        source.user = new User();
        Account target = Account.class.cast(source.clone());
        target.accountId = "target.accountId";
        target.password = "target.password";
        System.out.println(String.format("source:%s,target:%s", source.accountId, target.accountId));
        Assert.assertTrue(source.user == target.user);
    }


}