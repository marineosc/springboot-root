package com.young.mbgdiy.base;

import java.util.List;

/**
 * Base serviceImpl
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public abstract class BaseServiceImpl<T, Example extends BaseExample, ID> implements BaseService<T, Example, ID> {

    private BaseMapper<T, Example, ID> mapper;

    public void setMapper(BaseMapper<T, Example, ID> mapper) {
        this.mapper = mapper;
    }

    @Override
    public long countByExample(Example example) {
        return mapper.countByExample(example);
    }

    @Override
    public int deleteByExample(Example example) {
        return mapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(ID id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(T record) {
        return mapper.insert(record);
    }

    @Override
    public int insertSelective(T record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<T> selectByExample(Example example) {
        return mapper.selectByExample(example);
    }

    @Override
    public T selectByCondition(Example example) {

        List<T> datas = selectByExample(example);
        return datas != null && datas.size() == 0 ? null : datas.get(0);
    }

    /**
     * 自定义分页查询方法：
     *
     * @param example
     * @param pageInfo
     * @return
     */
    @Override
    public void selectByPageExample(Example example, PageInfo pageInfo) {
        if (pageInfo != null) {
            example.setPageInfo(pageInfo);
            pageInfo.setPageParams(Long.valueOf(this.countByExample(example)).intValue());
        }
        List<T> list = this.selectByExample(example);
        pageInfo.setList(list);
    }

    @Override
    public PageInfo<T> selectByPageExample(Example example, Integer currentPage, Integer pageSize) {
        PageInfo pageInfo = new PageInfo(currentPage, pageSize);
        example.setPageInfo(pageInfo);
        Integer count = Long.valueOf(this.countByExample(example)).intValue();
        pageInfo.setPageParams(count);
        Integer totalPage = (count / pageSize + ((count % pageSize == 0) ? 0 : 1));
        pageInfo.setTotalPage(totalPage);
        List<T> list = this.selectByExample(example);
        pageInfo.setList(list);
        return pageInfo;
    }

    @Override
    public T selectByPrimaryKey(ID id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(T record, Example example) {
        return mapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(T record, Example example) {
        return mapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(T record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(T record) {
        return mapper.updateByPrimaryKey(record);
    }
}
