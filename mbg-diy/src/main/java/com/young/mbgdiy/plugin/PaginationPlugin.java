package com.young.mbgdiy.plugin;

import com.young.mbgdiy.config.FullyQualifiedJavaTypeProxyFactory;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

import java.util.List;

/**
 * TODO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public class PaginationPlugin extends PluginAdapter {
    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelExampleClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

        FullyQualifiedJavaType baseExampleType = FullyQualifiedJavaTypeProxyFactory.getBaseExampleInstance();
        topLevelClass.setSuperClass(baseExampleType);
        topLevelClass.addImportedType(baseExampleType);
        return super.modelExampleClassGenerated(topLevelClass, introspectedTable);
    }

    @Override
    public boolean sqlMapSelectByExampleWithoutBLOBsElementGenerated(XmlElement element,
                                                                     IntrospectedTable introspectedTable) {
        XmlElement isNotNullElementGroupBy = new XmlElement("if");
        isNotNullElementGroupBy.addAttribute(new Attribute("test", "groupByClause != null"));
        isNotNullElementGroupBy.addElement(new TextElement("group by ${groupByClause}"));
        element.addElement(isNotNullElementGroupBy);

        XmlElement isNotNullElementPageInfo = new XmlElement("if");
        isNotNullElementPageInfo.addAttribute(new Attribute("test", "pageInfo != null"));
        isNotNullElementPageInfo.addElement(new TextElement("limit #{pageInfo.pageBegin} , #{pageInfo.pageSize}"));
        element.addElement(isNotNullElementPageInfo);
        return super.sqlMapSelectByExampleWithoutBLOBsElementGenerated(element, introspectedTable);
    }

    @Override
    public boolean sqlMapCountByExampleElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {

        XmlElement answer = new XmlElement("select");

        String fqjt = introspectedTable.getExampleType();

        answer.addAttribute(new Attribute("id", introspectedTable.getCountByExampleStatementId()));
        answer.addAttribute(new Attribute("parameterType", fqjt));
        answer.addAttribute(new Attribute("resultType", "java.lang.Integer"));

        this.context.getCommentGenerator().addComment(answer);

        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from ");
        sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());

        XmlElement ifElement = new XmlElement("if");
        ifElement.addAttribute(new Attribute("test", "_parameter != null"));
        XmlElement includeElement = new XmlElement("include");
        includeElement.addAttribute(new Attribute("refid", introspectedTable.getExampleWhereClauseId()));
        ifElement.addElement(includeElement);

        element.getElements().clear();
        element.getElements().add(new TextElement(sb.toString()));
        element.getElements().add(ifElement);
        return super.sqlMapUpdateByExampleWithoutBLOBsElementGenerated(element, introspectedTable);
    }
}
