package com.young.mbgdiy.plugin;

import com.young.mbgdiy.config.FullyQualifiedJavaTypeProxyFactory;
import com.young.mbgdiy.utils.CurrentDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.JavaTypeResolver;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * TODO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public class ClientDaoPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass,
                                   IntrospectedTable introspectedTable) {
        String author = this.getContext().getProperty("author");
        if (StringUtils.isEmpty(author)) {
            author = System.getProperty("user.name");
        }
        JavaTypeResolver javaTypeResolver = new JavaTypeResolverDefaultImpl();
        FullyQualifiedJavaType calculateJavaType = javaTypeResolver
                .calculateJavaType(introspectedTable.getPrimaryKeyColumns().get(0));

        FullyQualifiedJavaType superInterfaceType = new FullyQualifiedJavaType(
                new StringBuilder("BaseMapper<")
                        .append(introspectedTable.getBaseRecordType())
                        .append(",")
                        .append(introspectedTable.getExampleType())
                        .append(",")
                        .append(calculateJavaType.getShortName())
                        .append(">")
                        .toString()
        );
        FullyQualifiedJavaType baseMapperInstance = FullyQualifiedJavaTypeProxyFactory.getBaseMapperInstance();
        interfaze.addSuperInterface(superInterfaceType);
        interfaze.addImportedType(baseMapperInstance);
        interfaze.addAnnotation("@Repository");
        interfaze.addImportedType(new FullyQualifiedJavaType("org.springframework.stereotype.Repository"));
        interfaze.addJavaDocLine("/**");
        interfaze.addJavaDocLine("  *@author " + author);
        interfaze.addJavaDocLine("  *@date   " + CurrentDateUtil.getDate());
        interfaze.addJavaDocLine("  */");
        List<Method> changeMethods = new ArrayList<>();
        for (Method method : interfaze.getMethods()) {
            if (method.getName().endsWith("WithBLOBs")
                    || method.getReturnType().toString().endsWith("WithBLOBs")
                    || Arrays.toString(method.getParameters().toArray()).contains("WithBLOBs")) {
                changeMethods.add(method);
            }
        }

        interfaze.getMethods().retainAll(changeMethods);

        if (changeMethods.isEmpty()) {
            interfaze.getImportedTypes().removeIf(javaType -> javaType.getFullyQualifiedName().equals("java.util.List")
                    || javaType.getFullyQualifiedName().equals("org.apache.ibatis.annotations.Param"));
        }
        return super.clientGenerated(interfaze, topLevelClass, introspectedTable);
    }
}
