package com.young.simpleweb.controller;

import com.young.common.validation.ValidationGroup;
import com.young.simpleweb.entity.ParamUser;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Spring-validation
 * hibernate-validator-api
 * 参数校验测试用例
 *
 * @author ：young
 * @date ：Created in 2020/09/24
 * @description：简单请求
 */
@RestController
@RequestMapping(value = "validator")
@Validated
public class ValidatorController {
    /**
     * 接口参数校验：spring-validation
     * 分组{@link ValidationGroup.Insert}
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "paramCheckInsert", method = RequestMethod.POST)
    public String paramCheckInsert(@Validated(ValidationGroup.Insert.class) ParamUser user) {
        return user.toString();
    }

    /**
     * 接口参数校验：spring-validation
     * 分组{@link ValidationGroup.Update}
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "paramCheckUpdate", method = RequestMethod.POST)
    public String paramCheckUpdate(@Validated(ValidationGroup.Update.class) ParamUser user) {
        return user.toString();
    }

    /**
     * 接口参数校验：hibernate-validator
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "paramCheckValid", method = RequestMethod.POST)
    public String paramCheckValid(@Valid ParamUser user) {
        return user.toString();
    }
}
