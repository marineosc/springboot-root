package com.young.es.client.model;

import com.alibaba.fastjson.JSONObject;

/**
 * ElasticSearch Type Model
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/12/3
 */
public class DocumentModel {
    private static JSONObject properties;
    private static JSONObject id;
    private static JSONObject name;
    private static JSONObject content;

    static {
        properties = new JSONObject();
        id = new JSONObject();
        name = new JSONObject();
        content = new JSONObject();
        id.put("type", "long");
        id.put("store", "true");
        name.put("type", "text");
        name.put("index", "true");
        //name.put("analyzer", "ik_max_word");
        content.put("type", "text");
        content.put("index", "true");
        //content.put("analyzer", "ik_max_word");
        properties.put("id", id);
        properties.put("name", name);
        properties.put("content", content);
    }

    /**
     * 创建索引下类型方法
     *
     * @return
     */
    public static JSONObject getDocumentStruct() {
        JSONObject json = new JSONObject();
        json.put("properties", properties);
        return json;
    }

    /**
     * 生成数据
     *
     * @param id
     * @param name
     * @param content
     * @return
     */
    public static String getOneData(long id, String name, String content) {
        JSONObject value = new JSONObject();
        value.put("id", id);
        value.put("name", name);
        value.put("content", content);
        JSONObject json = new JSONObject();
        json.put("properties", value);
        return json.toJSONString();
    }

}
