package com.young.app.config;

import com.young.app.interceptor.CommonInterceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ：young
 * @date ：Created in 2019/7/19
 * @description：MVC配置
 */
@Component
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    private static Logger logger = LogManager.getLogger(WebMvcConfig.class);
    /**
     * 拦截器添加
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //此处可以添加多个拦截器
        //多个拦截器按照顺序执行
        registry.addInterceptor(new CommonInterceptor());
        //本地开发环境搞东西可以将认证类型的参数焊死
//        registry.addInterceptor(new DevSignValidateInterceptor());
    }
}
