package com.young.app.controller;

import com.young.common.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author ：young
 * @date ：Created in 2019/7/6
 * @description：somting
 */
@RestController
@RequestMapping(value = "/app")
public class FeignController {
    private static Logger logger = LogManager.getLogger(FeignController.class);

    @RequestMapping(value = "/feignDemo", method = RequestMethod.GET)
    public String feignDemo() {
        return "FeignDemo";
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String uploadfile(@RequestPart(value = "file") MultipartFile file) {
        logger.info("uploadFile filename={}", file.getOriginalFilename());
        return file.getOriginalFilename();
    }

    /**
     * 附件上传
     * 还需要JSONString反序列化
     * @param user
     * @param file
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/uploadattach", method = RequestMethod.POST)
    public String uploadattach(@RequestParam(value = "user") User user,@RequestParam(value = "file") MultipartFile file) throws IOException {
        logger.info("uploadFile filename={}", file.getOriginalFilename());
        byte[] cache = file.getBytes();
        //文件存储位置需要自定义
        File tmpFile = new File("/BAK_" + file.getOriginalFilename());
        OutputStream out = new FileOutputStream(tmpFile);
        out.write(cache);
        out.close();
        logger.info("uploadFile path={}", tmpFile.getPath());
        return file.getOriginalFilename();
    }
}
