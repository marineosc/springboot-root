package com.young.app.base;

/**
 * @author ：young
 * @date ：Created in 2019/7/25
 * @description：基于类初始化方式获取单例
 * 如果是对静态字段使用线程安全的延迟处理，建议
 * 使用基于类初始化的方案（基于JVM机制）。
 */
public class InstanceFactory {
    private static class InstanceHolder {
        public static Instance instance = new Instance();
    }

    public static Instance getInstance() {
        return InstanceHolder.instance;
    }
}
