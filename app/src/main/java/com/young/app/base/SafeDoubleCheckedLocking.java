package com.young.app.base;

/**
 * @author ：young
 * @date ：Created in 2019/7/25
 * @description：双重判断 除了对静态字段实现延迟初始化外，
 * 还可以对实例字段实现延迟初始化。
 * 降低类初始化或创建实例的开销。
 */
public class SafeDoubleCheckedLocking {
    private volatile static Instance instance;

    public static Instance getInstance() {
        if (instance == null) {
            synchronized (Instance.class) {
                if (instance == null) {
                    instance = new Instance();
                }
            }
        }
        return instance;
    }
}
