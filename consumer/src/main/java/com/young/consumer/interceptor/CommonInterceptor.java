package com.young.consumer.interceptor;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author ：young
 * @date ：Created in 2019/7/19
 * @description：通用拦截器
 */
public class CommonInterceptor extends HandlerInterceptorAdapter {
    private static Logger logger = LogManager.getLogger(CommonInterceptor.class);

    /**
     * 在方法被调用前执行。
     * 在该方法中可以做类似校验的功能。
     * 如果返回true，则继续调用下一个拦截器。
     * 如果返回false，则中断执行，也就是说我们想调用的方法不会被执行。
     * 但是你可以修改response为你想要的响应。
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        request.setAttribute("key", "com.young.consumer.interceptor.CommonInterceptor");
        return super.preHandle(request, response, handler);

    }

    /**
     * 在业务处理器处理请求完成之后，生成视图之前执行
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
    }

    /**
     * 在DispatcherServlet完全处理完请求之后被调用，可用于清理资源
     *
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        request.setAttribute("key", "com.young.consumer.interceptor.CommonInterceptor.afterCompletion");
    }
}
