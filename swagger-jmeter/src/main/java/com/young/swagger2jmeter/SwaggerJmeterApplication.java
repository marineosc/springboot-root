package com.young.swagger2jmeter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author young
 */
@EnableOpenApi
@SpringBootApplication
public class SwaggerJmeterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerJmeterApplication.class, args);
    }

}
