package com.young.proxytool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProxytoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProxytoolApplication.class, args);
    }

}
